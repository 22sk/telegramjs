'use strict';

let separators = {};

separators.Command = class Command {
  constructor(text) {
    var regex = /^\/([^@\s]+)@?(?:(\S+)|)\s?(.*)$/i;
    var command = regex.exec(text);
    if (command) {
      this.valid = true;
      this.text = text;
      this.command = command[1];
      this.bot = command[2];
      this.args = command[3];
    } else {
      this.valid = false;
    }
  }
};

module.exports = separators;
