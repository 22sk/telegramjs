'use strict';

const requires = require('../telegram/requires');
module.exports.handlers = [
  {
    label: 'Start command',
    requires: requires.command.is('start'),
    callback: (data, params, bot) => {
      bot.send('sendMessage', {
        chat_id: data.message.chat.id,
        text: 'Please replace `bot.handlers[0]` with a new handler.',
        parse_mode: 'Markdown'
      }, (error, response, body) => {
        console.log(body);
      });
    }
  },
  {
    label: 'Help command',
    requires: requires.command.is(['help', '?', 'man']),
    callback: (data, params, bot) => {
      bot.send('sendMessage', {
        chat_id: data.message.chat.id,
        text: 'Help command pending'
      }, (error, response, body) => {
        console.log(body);
      });
    }
  }
];
