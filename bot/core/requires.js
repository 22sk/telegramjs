'use strict';

var requires = {};

/**
 * e.g. `has('message', 'from', 'id')` checks if data has node message.from.id
 * @param nodes
 */
requires.has = (...nodes) => {
  return {
    label: 'Has '+nodes.join('.'),
    callback: (data) => {
      let before = data;
      for (let i=0; i<nodes.length; i++) {
        let node = nodes[i];
        if (!(node in before)) {
          return false;
        }
        before = before[node];
      }
      return true;
    }
  };
};

module.exports = requires;
