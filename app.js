'use strict';

const Bot = require('./bot');
const telegram = require('./bot/telegram');
const requires = require('./bot/telegram/requires');

let bot = new Bot(process.env.API_URL);
telegram.init(bot);

bot.register((data, params) => {
  bot.send('sendMessage', {
    chat_id: data.message.chat.id,
    text: params.command.args
  });
}, requires.command.is('echo'), 'Echo command');
